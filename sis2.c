#include <linux/module.h>       /* Needed by all modules */
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>       
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <asm/current.h>
#include <linux/stat.h>

static char *parameter = "all";
module_param(parameter, charp, 0000);

int print_total_cpu(void) {
struct sysinfo si;
// sysinfo(&si);
unsigned int eax=11,ebx=0,ecx=1,edx=0;

asm volatile("cpuid"
        : "=a" (eax),
          "=b" (ebx),
          "=c" (ecx),
          "=d" (edx)
        : "0" (eax), "2" (ecx)
        : );

printk("Cores: %d\nThreads: %d\nActual thread: %d\n",eax,ebx,edx);
printk("GGWP: %d", si.procs);
return ebx;
}

void filter_by_cpu(int x) {
    struct task_struct *p;
    printk(KERN_INFO "All processes for CPU %d:\n", x);
    for_each_process(p) {
        if(p->cpu == x) {
            printk(KERN_INFO "\t%s[%d].\n", p->comm, p->pid);
        }
    }
}

void print_processes(char *s) {
  printk(KERN_INFO "Function Starts:");
    int i = 0;
    struct task_struct *p;
    int cpus = print_total_cpu();
    if(s == "all") {
        for(i = 0; i < cpus; i++) {
            printk("All processes for CPU %d:\n", i);
            for_each_process(p) {
                if(p->cpu == i) {
                    printk(KERN_INFO "\t\t\t%s[%d].\n", p->comm, p->pid);
                }
            }
        }
    }
    
    printk(KERN_INFO "Function Ends.\n");
}

int init_module(void)
{
    printk(KERN_INFO "SIS 2 Start:\n");
    print_processes(parameter);
    return 0;
}

void cleanup_module(void)
{
    printk(KERN_INFO "SIS 2 Finish.\n");
}